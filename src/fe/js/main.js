import io from 'socket.io-client';

const socket = io('http://localhost:8080');

socket.on('problem', errorMsg => {
    alert(errorMsg.problem);
});

const $chatPanel = $('#chat-panel');
const $logoutCmd = $('#logout-cmd');

const $msgForm = $chatPanel.find('#msg-form');
const $msgText = $msgForm.find('#msg-text');

const $usernameList = $chatPanel.find('#username-list');
const $msgList = $chatPanel.find('#message-list');
const $nicknameInput = $('#nickname-input');

$chatPanel.hide();
const $nicknameForm = $('#nickname-form');
$nicknameForm.submit((event) => {
    const nickname = $nicknameInput.val();
    event.preventDefault();
    if (!nickname) {
        return;
    }
    socket.emit('connect-nick', {
        nickname: nickname
    });
});

socket.on('connected', data => {
    $nicknameForm.hide();
    $chatPanel.show();
});

$logoutCmd.click(() => {
    socket.emit('logout');
});

socket.on('logout', () => {
    $nicknameForm.show();
    $chatPanel.hide();
});

$msgForm.submit((event) => {
    event.preventDefault();
    const msg = $msgText.val();
    $msgText.val('');
    socket.emit('send-msg', {
        text: msg
    });
});

socket.on('usernames-update', data => {
    $usernameList.html('');
    const lis = data.forEach(username => {
        const $username = $(`<li>${username}</li>`);
        if (username === $nicknameInput.val()) {
            $username.css('color', 'blue');
        }
        $username.click(evt => {
            const privateMsg = window.prompt(`Send a private message to ${username}:`);
            socket.emit('send-private-msg', {
                text: privateMsg,
                dest: username
            });
        });
        $usernameList.append($username);
    });
});
socket.on('msg', data => {
    const $msg = $(`<li>
        <span>${data.sender}: </span>
        <span>${data.msg}</span>
    </li>`);
    if (data.isYou) {
        $msg.css('color', 'red');
    }
    
    if (data.isPrivate) {
        $msg.css('text-decoration', 'underline');
    }
    
    $msgList.append($msg);
});

$(window).on('unload', () => {
    socket.emit('logout');
});
