const app = require('http').createServer();
const io = require('socket.io')(app);

app.listen(8080);

const connections = [];

io.on('connection', socket => {
    let nickname;
    let connectionInfo;
    socket.on('connect-nick', data => {
        if (typeof(data.nickname) !== 'string') {
            socket.emit('problem', {problem: 'nickname must be a string'});
            return;
        }

        const connectionsFoundWithSameNick = connections.filter(connection => connection.nickname === data.nickname)
        if (connectionsFoundWithSameNick.length) {
            socket.emit('problem', {problem: 'nickname in use 2'});
            return;
        }

        if (connectionInfo) {
            socket.emit('problem', {problem: `already assigned (${connectionInfo.nickname})`});
            return;
        }
        
        connectionInfo = {
            nickname: data.nickname,
            socket: socket
        };
        connections.push(connectionInfo);

        nickname = data.nickname;
        
        socket.emit('connected', {
            connectedUsers: connections.map(connection => connection.nickname)
        });

        sendUpdatedUsers();
    });
    socket.on('logout', data => {

        const connectionIndex = connections.indexOf(connectionInfo);
        if (connectionIndex === -1) {
            return;
        }

        connections.splice(connectionIndex, 1);

        connectionInfo = null;
        socket.emit('logout');
        sendUpdatedUsers();
    });

    socket.on('send-private-msg', data => {
        connections.forEach(conn => {
            if (conn.nickname === data.dest || conn === connectionInfo) {
                conn.socket.emit('msg', {
                    sender: connectionInfo.nickname,
                    msg: data.text,
                    isYou: conn === connectionInfo,
                    isPrivate: true
                });
            }
        });
    });

    socket.on('send-msg', data => {
        connections.forEach(conn => {

            conn.socket.emit('msg', {
                sender: connectionInfo.nickname,
                msg: data.text,
                isYou: conn === connectionInfo,
                isPrivate: false
            });
        });
    });

    function sendUpdatedUsers () {
        const usernames = connections.map(connections => connections.nickname);
        connections.forEach(conn => {
            conn.socket.emit('usernames-update', usernames);
        });
    }
});
