const path = require('path'),
    HtmlWebpackPlugin = require('html-webpack-plugin'),
    ExtractTextPlugin = require('extract-text-webpack-plugin'),
    webpack = require("webpack");

module.exports = {
    entry: './src/fe/js/main.js',
    output: {
        filename: './main.boundle.js',
        path: path.resolve(__dirname, 'dist/fe'),
    },
    devtool: 'source-map',
    module: {
        rules: [{
            test: /.js$/,
            exclude: '/node_modules/',
            use: 'babel-loader'
        }]
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: 'Project Demo',
            hash: true,
            template: './src/fe/index.html'
        }),
        new ExtractTextPlugin({
            filename: 'app.css',
            disable: false,
            allChunks: true
        }),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery'
        })
    ]
    
};